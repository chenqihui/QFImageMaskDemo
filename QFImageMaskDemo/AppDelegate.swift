//
//  AppDelegate.swift
//  QFImageMaskDemo
//
//  Created by Anakin chen on 2020/3/12.
//  Copyright © 2020 QF Network Technology. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        let _ = QFScreenshotMan.shared
        return true
    }
}

