//
//  QFLSBOCMan.h
//  QFImageMaskDemo
//
//  Created by Anakin chen on 2020/3/15.
//  Copyright © 2020 QF Network Technology. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreVideo/CoreVideo.h>

NS_ASSUME_NONNULL_BEGIN

@interface QFLSBOCMan : NSObject

+ (UIImage *)imageFromPixelBuffer:(CVPixelBufferRef)pixelBufferRef;

+ (CVPixelBufferRef)pixelBufferFromCGImage:(CGImageRef)image;

@end

NS_ASSUME_NONNULL_END
